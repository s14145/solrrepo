package com.example.controller;

import com.example.entity.Employee;
import com.example.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/rest")
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @PostConstruct
    public void addEmployees(){
        List<Employee> employees=new ArrayList<>();
        employees.add(new Employee(5,"Tinu", new String[] {"Butwal","Kathmandu"}));
        employees.add(new Employee(1,"Binod",new String[]{"111 Travis Dr, Euless,TX","1327 Pearson Ave, San Leandro, CA"}));
        employees.add(new Employee(2,"Suresh",new String[]{"1453 Pearson Ave,San Leandro, CA","15280 East 4th Street, Lake Hills, WA"}));
        employees.add(new Employee(3,"Hans",new String[]{"222 Potrero Street, Aurora, CO","3600 PL , Bellevue, WA"}));
        employeeRepository.saveAll(employees);
    }

    @RequestMapping(value="/allEmployees",method= RequestMethod.GET)
    public Iterable<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }

    @RequestMapping(value="/allEmployees/{name}",method=RequestMethod.GET)
    public Employee findEmployeeByName(@PathVariable String name){
        return employeeRepository.findByName(name);
    }
}
