package com.example.repositories;

import com.example.entity.Employee;
import org.springframework.data.solr.repository.SolrCrudRepository;

public interface EmployeeRepository extends SolrCrudRepository<Employee,Integer> {
    Employee findByName(String name);
}
