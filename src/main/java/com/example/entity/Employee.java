package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@Data
@NoArgsConstructor
@SolrDocument(collection="employee") // Since this is a NoSQL and we are not going to store object instead key value pairs
public class Employee {

    @Id
    @Field
    private int id;
    @Field
    private String name;
    @Field
    private String[] address;

    public Employee(int id, String name, String[] address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
